/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.user;

import android.annotation.IntDef;
import android.os.Parcelable;

import com.android.internal.util.DataClass;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * User remove result.
 *
 * @hide
 */
@DataClass(
        genToString = true,
        genHiddenConstructor = true,
        genHiddenConstDefs = true)
public final class UserRemovalResult implements Parcelable {

    /**
     * When user remove is successful.
     *
     * @hide
     */
    public static final int STATUS_SUCCESSFUL = CommonResults.STATUS_SUCCESSFUL;

    /**
     * When user remove fails for android. Hal user is not removed.
     *
     * @hide
     */
    public static final int STATUS_ANDROID_FAILURE = CommonResults.STATUS_ANDROID_FAILURE;

    /**
     * When remove user fails for unknown error.
     *
     * @hide
     */
    public static final int STATUS_HAL_INTERNAL_FAILURE = CommonResults.STATUS_HAL_INTERNAL_FAILURE;

     /**
     * When user to remove is same as current user.
     *
     * @hide
     */
    public static final int STATUS_TARGET_USER_IS_CURRENT_USER =
            CommonResults.LAST_COMMON_STATUS + 1;

    /**
     * When user to remove doesn't exits.
     *
     * @hide
     */
    public static final int STATUS_USER_DOES_NOT_EXIST = CommonResults.LAST_COMMON_STATUS + 2;

    /**
     * When user to remove is last admin user.
     *
     * @hide
     */
    public static final int STATUS_TARGET_USER_IS_LAST_ADMIN_USER =
            CommonResults.LAST_COMMON_STATUS + 3;

    /**
     * Gets the user switch result status.
     *
     * @return either {@link UserRemovalResult#STATUS_SUCCESSFUL},
     *         {@link UserRemovalResult#STATUS_ANDROID_FAILURE},
     *         {@link UserRemovalResult#STATUS_HAL_INTERNAL_FAILURE},
     *         {@link UserRemovalResult#STATUS_TARGET_USER_IS_CURRENT_USER},
     *         {@link UserRemovalResult#STATUS_USER_DOES_NOT_EXIST}, or
     *         {@link UserRemovalResult#STATUS_TARGET_USER_IS_LAST_ADMIN_USER}.
     */
    private final @Status int mStatus;

    public boolean isSuccess() {
        return mStatus == STATUS_SUCCESSFUL;
    }

    // TODO(b/158195639): if you change any status constant, you need to manually assign its values
    // (rather than using CommonResults) before running codegen to regenerate the class


    // Code below generated by codegen v1.0.15.
    //
    // DO NOT MODIFY!
    // CHECKSTYLE:OFF Generated code
    //
    // To regenerate run:
    // $ codegen $ANDROID_BUILD_TOP/packages/services/Car/car-lib/src/android/car/user/UserRemovalResult.java
    //
    // To exclude the generated code from IntelliJ auto-formatting enable (one-time):
    //   Settings > Editor > Code Style > Formatter Control
    //@formatter:off


    /** @hide */
    @IntDef(prefix = "STATUS_", value = {
        STATUS_SUCCESSFUL,
        STATUS_ANDROID_FAILURE,
        STATUS_HAL_INTERNAL_FAILURE,
        STATUS_TARGET_USER_IS_CURRENT_USER,
        STATUS_USER_DOES_NOT_EXIST,
        STATUS_TARGET_USER_IS_LAST_ADMIN_USER
    })
    @Retention(RetentionPolicy.SOURCE)
    @DataClass.Generated.Member
    public @interface Status {}

    /** @hide */
    @DataClass.Generated.Member
    public static String statusToString(@Status int value) {
        switch (value) {
            case STATUS_SUCCESSFUL:
                    return "STATUS_SUCCESSFUL";
            case STATUS_ANDROID_FAILURE:
                    return "STATUS_ANDROID_FAILURE";
            case STATUS_HAL_INTERNAL_FAILURE:
                    return "STATUS_HAL_INTERNAL_FAILURE";
            case STATUS_TARGET_USER_IS_CURRENT_USER:
                    return "STATUS_TARGET_USER_IS_CURRENT_USER";
            case STATUS_USER_DOES_NOT_EXIST:
                    return "STATUS_USER_DOES_NOT_EXIST";
            case STATUS_TARGET_USER_IS_LAST_ADMIN_USER:
                    return "STATUS_TARGET_USER_IS_LAST_ADMIN_USER";
            default: return Integer.toHexString(value);
        }
    }

    /**
     * Creates a new UserRemovalResult.
     *
     * @param status
     *   Gets the user switch result status.
     *
     *   @return either {@link UserRemovalResult#STATUS_SUCCESSFUL},
     *           {@link UserRemovalResult#STATUS_ANDROID_FAILURE},
     *           {@link UserRemovalResult#STATUS_HAL_INTERNAL_FAILURE},
     *           {@link UserRemovalResult#STATUS_TARGET_USER_IS_CURRENT_USER},
     *           {@link UserRemovalResult#STATUS_USER_DOES_NOT_EXIST}, or
     *           {@link UserRemovalResult#STATUS_TARGET_USER_IS_LAST_ADMIN_USER}.
     * @hide
     */
    @DataClass.Generated.Member
    public UserRemovalResult(
            int status) {
        this.mStatus = status;

        // onConstructed(); // You can define this method to get a callback
    }

    /**
     * Gets the user switch result status.
     *
     * @return either {@link UserRemovalResult#STATUS_SUCCESSFUL},
     *         {@link UserRemovalResult#STATUS_ANDROID_FAILURE},
     *         {@link UserRemovalResult#STATUS_HAL_INTERNAL_FAILURE},
     *         {@link UserRemovalResult#STATUS_TARGET_USER_IS_CURRENT_USER},
     *         {@link UserRemovalResult#STATUS_USER_DOES_NOT_EXIST}, or
     *         {@link UserRemovalResult#STATUS_TARGET_USER_IS_LAST_ADMIN_USER}.
     */
    @DataClass.Generated.Member
    public int getStatus() {
        return mStatus;
    }

    @Override
    @DataClass.Generated.Member
    public String toString() {
        // You can override field toString logic by defining methods like:
        // String fieldNameToString() { ... }

        return "UserRemovalResult { " +
                "status = " + mStatus +
        " }";
    }

    @Override
    @DataClass.Generated.Member
    public void writeToParcel(@android.annotation.NonNull android.os.Parcel dest, int flags) {
        // You can override field parcelling by defining methods like:
        // void parcelFieldName(Parcel dest, int flags) { ... }

        dest.writeInt(mStatus);
    }

    @Override
    @DataClass.Generated.Member
    public int describeContents() { return 0; }

    /** @hide */
    @SuppressWarnings({"unchecked", "RedundantCast"})
    @DataClass.Generated.Member
    /* package-private */ UserRemovalResult(@android.annotation.NonNull android.os.Parcel in) {
        // You can override field unparcelling by defining methods like:
        // static FieldType unparcelFieldName(Parcel in) { ... }

        int status = in.readInt();

        this.mStatus = status;

        // onConstructed(); // You can define this method to get a callback
    }

    @DataClass.Generated.Member
    public static final @android.annotation.NonNull Parcelable.Creator<UserRemovalResult> CREATOR
            = new Parcelable.Creator<UserRemovalResult>() {
        @Override
        public UserRemovalResult[] newArray(int size) {
            return new UserRemovalResult[size];
        }

        @Override
        public UserRemovalResult createFromParcel(@android.annotation.NonNull android.os.Parcel in) {
            return new UserRemovalResult(in);
        }
    };

    @DataClass.Generated(
            time = 1591259644931L,
            codegenVersion = "1.0.15",
            sourceFile = "packages/services/Car/car-lib/src/android/car/user/UserRemovalResult.java",
            inputSignatures = "public static final  int STATUS_SUCCESSFUL\npublic static final  int STATUS_ANDROID_FAILURE\npublic static final  int STATUS_HAL_INTERNAL_FAILURE\npublic static final  int STATUS_TARGET_USER_IS_CURRENT_USER\npublic static final  int STATUS_USER_DOES_NOT_EXIST\npublic static final  int STATUS_TARGET_USER_IS_LAST_ADMIN_USER\nprivate final  int mStatus\npublic  boolean isSuccess()\nclass UserRemovalResult extends java.lang.Object implements [android.os.Parcelable]\n@com.android.internal.util.DataClass(genToString=true, genHiddenConstructor=true, genHiddenConstDefs=true)")
    @Deprecated
    private void __metadata() {}


    //@formatter:on
    // End of generated code

}
